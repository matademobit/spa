import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../servicios/heroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: any;

  constructor(private heroesService:HeroesService, private router:Router) { }

  ngOnInit() {
    
   // this.heroes = this.heroesService.getHeroes();
   this.heroesService.getHeroesAPI()
   .subscribe(data => {
     console.log(data)
     console.log(data.results)
     this.heroes = data.results;
   },
      error => {
        console.log("fallo el call de la API");
      
        console.log(error)
      });
 
     console.log(this.heroes);

   
    
  }

  verHeroe(id:number){

    console.log('imprimo paramaetro .ts:'+id);
    this.router.navigate(['/heroe',id]);

    
  }

}
