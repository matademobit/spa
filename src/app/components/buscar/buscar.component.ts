import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { HeroesService } from "../servicios/heroes.service";

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {

  heroes:any;

  constructor(private activatedRouted:ActivatedRoute, private heroesService:HeroesService) {

    this.activatedRouted.params.subscribe(params => {
      console.log('Parametro recibido:'+params['term']);
      this.heroes = this.heroesService.buscarHeroe(params['term']);
      console.log(this.heroes);
      
    })
   }

  ngOnInit() {
  }

}
