import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {HeroesComponent} from './components/heroes/heroes.component';
import {AboutComponent} from './components/about/about.component';
import {HeroeComponent} from './components/heroe/heroe.component';
import {BuscarComponent} from './components/buscar/buscar.component';
import {CrearheroeComponent} from './components/crearheroe/crearheroe.component';

const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'heroes', component: HeroesComponent },
    { path: 'about', component: AboutComponent },
    { path: 'crearheroe', component: CrearheroeComponent },
    { path: 'heroe/:id', component: HeroeComponent },
    { path: 'buscar/:term', component: BuscarComponent },
    { path: '**', pathMatch:'full', redirectTo: 'home' }
];

export const appRouting = RouterModule.forRoot(routes);